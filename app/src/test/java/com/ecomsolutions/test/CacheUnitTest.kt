package com.ecomsolutions.test

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.ecomsolutions.test.cache.AppDataBase
import com.ecomsolutions.test.cache.entities.UserDTO
import com.ecomsolutions.test.core.API
import com.ecomsolutions.test.core.Mappers
import com.ecomsolutions.test.core.Repositories
import com.ecomsolutions.test.features.users.data.ResultItem
import com.ecomsolutions.test.features.users.data.UserMapperImpl
import com.ecomsolutions.test.features.users.data.UserRepositoryImpl
import com.ecomsolutions.test.features.users.domain.User
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

/**
 * Тест для класса AppDataBase
 * @author Pavel Sarpov
 */
@RunWith(AndroidJUnit4::class)
class CacheUnitTest : BaseTest() {

    private val apiMock = mock<API>()

    private val userResponseMock =
        ResultItem.UserResponse(
            name = ResultItem.UserResponse.Name("First", "Last"),
            picture = ResultItem.UserResponse.Picture("avatar-url")
        )
    private val userMock = User(fullName = "First Last", avatarUrl = "avatar-url")
    private val userResponseMockList = ResultItem(listOf(userResponseMock))
    private val userDTOMock = UserDTO(
        UUID.randomUUID().toString(), "First Last", " avatar -url"
    )

    private lateinit var repository: Repositories.UserRepository
    private lateinit var userMapper: Mappers.UserMapper
    private lateinit var cache: AppDataBase

    @Before
    override fun setUp() {
        super.setUp()
        val context = InstrumentationRegistry.getInstrumentation().context
        cache = Room.databaseBuilder(context, AppDataBase::class.java, "database").build()
        userMapper = UserMapperImpl()
        repository = UserRepositoryImpl(userMapper, apiMock, cache)
    }

    @Test
    fun `test cache save data`() {
        runBlocking {
            withContext(Dispatchers.Default) {
                cache.userDao().delete()
                val usersAfterDelete = cache.userDao().getUsers()
                assert(usersAfterDelete.isEmpty())
                cache.userDao().insertUsers(listOf(userDTOMock))
                val usersAfterInsert = cache.userDao().getUsers()
                assert(usersAfterInsert.isNotEmpty())
            }
        }
    }
}