package com.ecomsolutions.test

import com.ecomsolutions.test.cache.AppDataBase
import com.ecomsolutions.test.cache.dao.UserDao
import com.ecomsolutions.test.core.API
import com.ecomsolutions.test.core.Mappers
import com.ecomsolutions.test.core.Repositories
import com.ecomsolutions.test.features.users.data.ResultItem
import com.ecomsolutions.test.features.users.data.UserRepositoryImpl
import com.ecomsolutions.test.features.users.domain.User
import com.nhaarman.mockitokotlin2.doNothing
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Maybe
import io.reactivex.observers.TestObserver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

/**
 * Тест для класса UserRepositoryImpl
 * @author Pavel Sarpov
 */
class UserRepositoryUnitTest : BaseTest() {

    @Mock
    lateinit var apiMock: API

    val cacheMock = mock<AppDataBase>()
    val userDaoMock = mock<UserDao>()

    @Mock
    lateinit var mapperMock: Mappers.UserMapper

    lateinit var repository: Repositories.UserRepository

    private val userResponseMock =
        ResultItem.UserResponse(
            name = ResultItem.UserResponse.Name("First", "Last"),
            picture = ResultItem.UserResponse.Picture("avatar-url")
        )
    private val userMock = User(fullName = "First Last", avatarUrl = "avatar-url")
    private val userResponseMockList = ResultItem(listOf(userResponseMock))

    @Before
    override fun setUp() {
        super.setUp()
        repository = UserRepositoryImpl(mapperMock, apiMock, cacheMock)
    }

    @Test
    fun `test repository load users with elements`() {
        whenever(cacheMock.userDao()).thenReturn(userDaoMock)
        doNothing().`when`(userDaoMock).delete()
        doNothing().`when`(userDaoMock).insertUsers(any())
        whenever(apiMock.getUsers(Mockito.anyInt(), Mockito.anyInt())).thenReturn(
            Maybe.just(
                userResponseMockList
            )
        )
        whenever(mapperMock.toEntity(userResponseMockList.results)).thenReturn(listOf(userMock))

        val subscriber = TestObserver<Any>()

        val observable = repository.loadUsers(Mockito.anyInt(), Mockito.anyInt(), true)

        observable.subscribe(subscriber)

        subscriber.assertComplete()
        subscriber.assertNoErrors()
        subscriber.assertValues(listOf(userMock))
    }

    @Test
    fun `test repository load empty list`() {
        whenever(cacheMock.userDao()).thenReturn(userDaoMock)
        doNothing().`when`(userDaoMock).delete()
        doNothing().`when`(userDaoMock).insertUsers(any())
        whenever(apiMock.getUsers(Mockito.anyInt(), Mockito.anyInt())).thenReturn(
            Maybe.just(
                userResponseMockList
            )
        )
        whenever(mapperMock.toEntity(userResponseMockList.results)).thenReturn(emptyList())

        val subscriber = TestObserver<Any>()

        val observable = repository.loadUsers(Mockito.anyInt(), Mockito.anyInt(), true)

        observable.subscribe(subscriber)

        subscriber.assertComplete()
        subscriber.assertNoErrors()
        subscriber.assertValues(emptyList<User>())
    }

    @Test
    fun `test ignoring cache true`() {
        whenever(apiMock.getUsers(Mockito.anyInt(), Mockito.anyInt())).thenReturn(
            Maybe.just(
                userResponseMockList
            )
        )
        val subscriber = TestObserver<Any>()
        runBlocking {
            withContext(Dispatchers.Default) {
                val observable = repository.loadUsers(Mockito.anyInt(), Mockito.anyInt(), true)
                observable.subscribe(subscriber)
                verify(userDaoMock, Mockito.times(0)).getUsers()
            }
        }
    }
}