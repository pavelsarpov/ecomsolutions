package com.ecomsolutions.test

import com.ecomsolutions.test.core.Mappers
import com.ecomsolutions.test.features.users.data.ResultItem
import com.ecomsolutions.test.features.users.data.UserMapperImpl
import org.junit.Before
import org.junit.Test

/**
 * Тест для класса UserMapperImpl
 * @author Pavel Sarpov
 */
class UserMapperUnitTest : BaseTest() {

    lateinit var mapper: Mappers.UserMapper
    private val userResponseMock =
        ResultItem.UserResponse(name = ResultItem.UserResponse.Name("First", "Last"), picture = ResultItem.UserResponse.Picture("avatar-url"))

    @Before
    override fun setUp() {
        super.setUp()
        mapper = UserMapperImpl()
    }

    @Test
    fun `test user mapper ok`() {
        val from = userResponseMock
        val result = mapper.toEntity(from)
        with(result) {
            assert(avatarUrl == from.picture.thumbnail && fullName == "${from.name.first} ${from.name.last}")
        }
    }
}