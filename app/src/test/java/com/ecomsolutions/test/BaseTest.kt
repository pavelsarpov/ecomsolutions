package com.ecomsolutions.test

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

open class BaseTest {

    @Before
    open fun setUp() {
        prepareSchedulers()
        MockitoAnnotations.initMocks(this); //without this you will get NPE
    }

    private fun prepareSchedulers() {
        val immediateScheduler = Schedulers.from(Runnable::run)
        RxJavaPlugins.setIoSchedulerHandler {
            immediateScheduler
        }
        RxJavaPlugins.setComputationSchedulerHandler {
            immediateScheduler
        }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler {
            immediateScheduler
        }
    }

    fun <T> any(): T {
        Mockito.any<T>()
        return null as T
    }
}