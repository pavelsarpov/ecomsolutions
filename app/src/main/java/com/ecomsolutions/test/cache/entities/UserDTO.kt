package com.ecomsolutions.test.cache.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserDTO(
    @PrimaryKey
    val id: String,
    val fullName: String,
    val avatarUrl: String
)