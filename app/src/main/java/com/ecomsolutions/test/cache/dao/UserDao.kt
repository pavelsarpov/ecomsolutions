package com.ecomsolutions.test.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ecomsolutions.test.cache.entities.UserDTO

@Dao
interface UserDao {
    @Query("SELECT * FROM userdto")
    fun getUsers(): List<UserDTO>

    @Insert
    fun insertUsers(users: List<UserDTO>)

    @Query("DELETE FROM userdto")
    fun delete()
}