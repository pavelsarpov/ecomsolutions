package com.ecomsolutions.test.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ecomsolutions.test.cache.dao.UserDao
import com.ecomsolutions.test.cache.entities.UserDTO

@Database(entities = [UserDTO::class], version = 1)
abstract class AppDataBase : RoomDatabase() {
    abstract fun userDao(): UserDao
}