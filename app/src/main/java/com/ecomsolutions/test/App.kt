package com.ecomsolutions.test

import com.ecomsolutions.test.di.components.DaggerAppComponent
import com.ecomsolutions.test.di.modules.ApplicationModule
import com.ecomsolutions.test.di.modules.PersistenceModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber
import timber.log.Timber.DebugTree

class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<App> {
        return DaggerAppComponent.builder()
                .applicationModule(ApplicationModule(this))
                .persistenceModule(PersistenceModule(this.applicationContext))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }
}