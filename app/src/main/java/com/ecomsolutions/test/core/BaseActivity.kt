package com.ecomsolutions.test.core

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import dagger.android.support.DaggerAppCompatActivity
import com.ecomsolutions.test.R

abstract class BaseActivity<State : Any, Event : Any> : DaggerAppCompatActivity() {

    abstract val layoutRes: Int
    abstract fun provideBaseViewModel(): BaseViewModel<State, Event>?
    abstract fun handleState(state: State)
    abstract fun handleEvent(event: Event)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
        provideBaseViewModel()?.stateHolder()
            ?.observe(this, Observer { state -> handleState(state) })
        provideBaseViewModel()?.eventHolder()
            ?.observe(this, Observer { event -> handleEvent(event) })
    }

    fun loadDialogFragment(f: DialogFragment) {
        f.show(supportFragmentManager, f.tag)
    }

    fun loadFragment(f: Fragment, addToBackStack: Boolean) {
        if (addToBackStack) {
            supportFragmentManager
                .beginTransaction()
                .addToBackStack(f.tag)
                .replace(R.id.container, f).commit()
        } else {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, f).commit()
        }
    }
}
