package com.ecomsolutions.test.core

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.ecomsolutions.test.R
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable

/**
 * BaseFragment should be extended by every fragment in app.
 * It accepts parameter fullscreen via constructor which will
 * cause the app to show either below status bar or not.
 */
abstract class BaseFragment<State : Any, Event : Any> : DaggerFragment() {

    abstract val layoutRes: Int
    abstract fun provideBaseViewModel(): BaseViewModel<State, Event>?
    abstract fun handleState(state: State)
    abstract fun handleEvent(event: Event)
    private val disposables = CompositeDisposable()

    private lateinit var dialog: AlertDialog
    private lateinit var mainActivity: DaggerAppCompatActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(layoutRes, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        provideBaseViewModel()?.stateHolder()
            ?.observe(viewLifecycleOwner, Observer { state -> handleState(state) })
        provideBaseViewModel()?.eventHolder()
            ?.observe(viewLifecycleOwner, Observer { event -> handleEvent(event) })
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    internal fun showNeutralDialog(message: String) {
        if (this::dialog.isInitialized && dialog.isShowing) {
            dialog.dismiss()
            showNeutralDialog(message)
        } else {
            dialog = AlertDialog.Builder(requireContext())
                .setMessage(message)
                .setCancelable(true)
                .setNeutralButton(requireContext().resources.getString(R.string.ok)) { dialog, _ -> dialog.dismiss() }
                .show()
        }
    }

    internal fun showDialog(message: String, positiveFunc: () -> Unit) {
        if (this::dialog.isInitialized && dialog.isShowing) {
            dialog.dismiss()
            showDialog(message, positiveFunc)
        } else {
            dialog = AlertDialog.Builder(requireContext())
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(requireContext().resources.getString(R.string.yes)) { dialog, _ ->
                    positiveFunc()
                    dialog.dismiss()
                }
                .setNegativeButton(requireContext().resources.getString(R.string.no)) { dialog, _ -> dialog.dismiss() }
                .show()
        }
    }
}
