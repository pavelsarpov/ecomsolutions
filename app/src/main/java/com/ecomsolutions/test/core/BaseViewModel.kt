@file:Suppress("PropertyName")

package com.ecomsolutions.test.core

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ecomsolutions.test.core.shared.architecture.CommonState
import com.ecomsolutions.test.core.shared.architecture.ErrorState
import com.ecomsolutions.test.core.shared.architecture.LiveEvent
import com.ecomsolutions.test.core.shared.architecture.LoadingState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel<State : Any, Event : Any> : ViewModel() {
    private val stateLiveData: MutableLiveData<State> = MutableLiveData()
    private val commonStateLiveData: MutableLiveData<CommonState> = MutableLiveData()
    private val eventLiveData: LiveEvent<Event> = LiveEvent()
    val disposables = CompositeDisposable()

    fun stateHolder(): LiveData<State> = stateLiveData
    fun commonStateHolder(): LiveData<CommonState> = commonStateLiveData
    fun eventHolder(): LiveData<Event> = eventLiveData

    protected var viewState: State?
        get() = stateLiveData.value
        set(value) {
            stateLiveData.value = value
        }

    private var commonState: CommonState
        get() = commonStateLiveData.value ?: CommonState()
        set(value) {
            commonStateLiveData.value = value
        }

    protected fun emitEvent(event: Event) {
        eventLiveData.value = event
    }

    protected fun showLoading() {
        commonState = commonState.copy(loadingState = LoadingState.Loading)
    }

    protected fun hideLoading() {
        commonState = commonState.copy(loadingState = LoadingState.Idle)
    }

    protected fun showError(message: String = "") {
        commonState = commonState.copy(errorState = ErrorState.Error(message))
    }

    protected fun showUnknownError() {
        commonState = commonState.copy(errorState = ErrorState.UnknownError)
    }

    fun dismissError() {
        commonState = commonState.copy(errorState = ErrorState.NoError)
    }

    fun clearCommonState() {
        commonState = CommonState()
    }

    operator fun Disposable.plusAssign(disposable: Disposable) {
        disposables.add(disposable)
    }

    fun Disposable.toDisposables() {
        disposables.add(this)
    }

    override fun onCleared() {
        disposables.clear()
    }
}
