package com.ecomsolutions.test.core

import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.loadImage(imageURL: String) {
    Picasso.get().load(imageURL).into(this)
}