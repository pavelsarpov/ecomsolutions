package com.ecomsolutions.test.core

import com.ecomsolutions.test.features.users.data.ResultItem
import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Query

interface API {
    @GET("api/")
    fun getUsers(@Query("page") page: Int, @Query("results") results: Int): Maybe<ResultItem>
}