package com.ecomsolutions.test.core

import com.ecomsolutions.test.cache.entities.UserDTO
import com.ecomsolutions.test.features.users.data.ResultItem
import com.ecomsolutions.test.features.users.domain.User

interface BaseMapper {
    interface ToEntity<in Response, Entity> {
        fun toEntity(item: Response): Entity

        fun toEntity(items: List<Response>): List<Entity> =
            when (items.isEmpty()) {
                true -> listOf()
                false -> items.map { toEntity(it) }
            }
    }

    interface ToDTO<in Entity, DTO> {
        fun toDTO(item: Entity): DTO

        fun toDTO(items: List<Entity>): List<DTO> =
            when (items.isEmpty()) {
                true -> listOf()
                false -> items.map { toDTO(it) }
            }
    }

    interface FromDTO<in DTO, Entity> {
        fun fromDTO(item: DTO): Entity

        fun fromDTO(items: List<DTO>): List<Entity> =
            when (items.isEmpty()) {
                true -> listOf()
                false -> items.map { fromDTO(it) }
            }
    }

    interface ToRequest<in Entity, Request> {

        fun toRequest(item: Entity): Request

        fun toRequest(items: List<Entity>): List<Request> =
            when (items.isEmpty()) {
                true -> listOf()
                false -> items.map { toRequest(it) }
            }
    }
}

interface Mappers {
    interface UserMapper : BaseMapper.ToEntity<ResultItem.UserResponse, User>, BaseMapper.ToDTO<User, UserDTO>,
        BaseMapper.FromDTO<UserDTO, User>

}