package com.ecomsolutions.test.core

import com.ecomsolutions.test.features.users.domain.User
import io.reactivex.Maybe

interface Interactors {
    interface UserInteractor {
        fun loadUsers(page: Int, amount: Int, ignoringCache: Boolean): Maybe<List<User>>
    }
}