package com.ecomsolutions.test.features.users

import com.ecomsolutions.test.di.providers.InjectionViewModelProvider
import com.ecomsolutions.test.di.qualifiers.ViewModelInjection
import com.ecomsolutions.test.features.users.ui.UserViewModel
import com.ecomsolutions.test.features.users.ui.UsersFragment
import dagger.Module
import dagger.Provides

@Module
class UserModule {

    @Provides
    @ViewModelInjection
    fun viewModel(
        provider: InjectionViewModelProvider<UserViewModel>,
        fragment: UsersFragment
    ): UserViewModel = provider.provide(fragment)
}