package com.ecomsolutions.test.features.users.data

import com.google.gson.annotations.SerializedName

data class ResultItem(
    @SerializedName("results")
    val results: List<UserResponse>
) {
    data class UserResponse(
        @SerializedName("name")
        val name: Name,
        @SerializedName("picture")
        val picture: Picture
    ) {
        data class Name(
            @SerializedName("first")
            val first: String,
            @SerializedName("last")
            val last: String
        )

        data class Picture(
            @SerializedName("thumbnail")
            val thumbnail: String
        )
    }
}



