package com.ecomsolutions.test.features.users.ui

import com.ecomsolutions.test.features.users.domain.User

sealed class UserState {
    data class UsersLoaded(val users: List<User>) : UserState()

}

sealed class UserEvent {
    object UserLoadingFailed : UserEvent()
    object UsersEmpty : UserEvent()
    object StartLoading : UserEvent()
    object FinishLoading : UserEvent()
}