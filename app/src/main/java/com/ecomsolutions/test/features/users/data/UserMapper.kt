package com.ecomsolutions.test.features.users.data

import com.ecomsolutions.test.cache.entities.UserDTO
import com.ecomsolutions.test.core.Mappers
import com.ecomsolutions.test.features.users.domain.User
import java.util.*
import javax.inject.Inject

class UserMapperImpl @Inject constructor() : Mappers.UserMapper {
    override fun toEntity(item: ResultItem.UserResponse): User {
        return User(
            fullName = "${item.name.first} ${item.name.last}",
            avatarUrl = item.picture.thumbnail
        )
    }

    override fun toDTO(item: User): UserDTO = UserDTO(
        id = UUID.randomUUID().toString(),
        fullName = item.fullName,
        avatarUrl = item.avatarUrl
    )

    override fun fromDTO(item: UserDTO): User =
        User(fullName = item.fullName, avatarUrl = item.avatarUrl)
}