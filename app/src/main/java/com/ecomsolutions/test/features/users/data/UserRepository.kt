package com.ecomsolutions.test.features.users.data

import com.ecomsolutions.test.cache.AppDataBase
import com.ecomsolutions.test.core.API
import com.ecomsolutions.test.core.Mappers
import com.ecomsolutions.test.core.Repositories
import com.ecomsolutions.test.features.users.domain.User
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepositoryImpl @Inject constructor(
        private val userMapper: Mappers.UserMapper,
        private val api: API,
        private val cache: AppDataBase
) :
        Repositories.UserRepository {
    override fun loadUsers(page: Int, amount: Int, ignoringCache: Boolean): Maybe<List<User>> {
        return if (ignoringCache) {
            loadUsersFromServer(page, amount)
        } else {
            loadUsersFromCache().flatMap {
                if (it.isEmpty()) {
                    loadUsersFromServer(page, amount)
                } else {
                    Maybe.just(it)
                }
            }
        }
    }

    private fun loadUsersFromCache(): Maybe<List<User>> =
            Maybe.fromCallable { cache.userDao().getUsers() }
                    .map { userMapper.fromDTO(it) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    private fun loadUsersFromServer(page: Int, amount: Int): Maybe<List<User>> =
            api.getUsers(page, amount)
                    .map {
                        val userResponses = it.results.map { it }
                        val users = userMapper.toEntity(userResponses)
                        cache.userDao().delete()
                        cache.userDao().insertUsers(userMapper.toDTO(users))
                        users
                    }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
}