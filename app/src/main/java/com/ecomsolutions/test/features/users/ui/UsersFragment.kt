package com.ecomsolutions.test.features.users.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecomsolutions.test.R
import com.ecomsolutions.test.core.BaseFragment
import com.ecomsolutions.test.databinding.FragmentUsersBinding
import com.ecomsolutions.test.di.qualifiers.ViewModelInjection
import com.ecomsolutions.test.features.users.domain.User
import javax.inject.Inject

class UsersFragment : BaseFragment<UserState, UserEvent>() {


    @Inject
    @ViewModelInjection
    lateinit var viewModel: UserViewModel

    private lateinit var binding: FragmentUsersBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentUsersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        retainInstance = true
        viewModel.loadUsers()
        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.forceLoadUsers(ignoringCache = true)
        }
    }

    override val layoutRes: Int = R.layout.fragment_users

    override fun provideBaseViewModel() = viewModel

    override fun handleState(state: UserState) {
        when (state) {
            is UserState.UsersLoaded -> renderUsers(state.users)
        }
    }

    private fun renderUsers(users: List<User>) {
        val usersRecyclerView = UsersAdapter(users)
        with(binding.usersRecyclerView) {
            visibility = View.VISIBLE
            adapter = usersRecyclerView
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext())
        }
        usersRecyclerView.notifyDataSetChanged()
    }

    override fun handleEvent(event: UserEvent) {
        when (event) {
            UserEvent.UserLoadingFailed -> showNeutralDialog(getString(R.string.user_loading_failed))
            UserEvent.UsersEmpty -> showNeutralDialog(getString(R.string.user_empty))
            UserEvent.StartLoading -> startLoading()
            UserEvent.FinishLoading -> finishLoading()
        }
    }

    private fun startLoading() {
        binding.progress.visibility = View.VISIBLE
        binding.usersRecyclerView.visibility = View.GONE
    }

    private fun finishLoading() {
        binding.progress.visibility = View.GONE
        binding.usersRecyclerView.visibility = View.VISIBLE
        binding.swipeRefreshLayout.isRefreshing = false
    }
}