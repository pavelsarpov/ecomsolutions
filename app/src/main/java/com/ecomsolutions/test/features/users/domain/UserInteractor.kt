package com.ecomsolutions.test.features.users.domain

import com.ecomsolutions.test.core.Interactors
import com.ecomsolutions.test.core.Repositories
import io.reactivex.Maybe
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserInteractorImpl @Inject constructor(private val repository: Repositories.UserRepository) : Interactors.UserInteractor {
    override fun loadUsers(page: Int, amount: Int, ignoringCache: Boolean): Maybe<List<User>> = repository.loadUsers(page, amount, ignoringCache)
}