package com.ecomsolutions.test.features.users.ui

import com.ecomsolutions.test.core.BaseViewModel
import com.ecomsolutions.test.core.Interactors
import com.ecomsolutions.test.features.users.domain.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserViewModel @Inject constructor(
    private val interactor: Interactors.UserInteractor
) : BaseViewModel<UserState, UserEvent>() {

    private val page = 1
    private val amount = 15
    private val users = mutableListOf<User>()

    /**
     * Делает проверку на наличие данных в users
     * Если данные не найдены - идет в forceLoadUsers
     * Если найдены - передает их во viewState
     */
    fun loadUsers() {
        if (users.isNotEmpty()) {
            viewState = UserState.UsersLoaded(users)
        } else {
            forceLoadUsers()
        }
    }

    /**
     * Выполняет непосредственный запрос в интерактор на загрузку данных
     * Если данные получены успешно - перезаписывает их в users
     */
    fun forceLoadUsers(ignoringCache: Boolean = false) {
        emitEvent(UserEvent.StartLoading)
        interactor.loadUsers(page, amount, ignoringCache)
            .subscribe({
                emitEvent(UserEvent.FinishLoading)
                viewState = UserState.UsersLoaded(it)
                users.clear()
                users.addAll(it)
            }, {
                it.printStackTrace()
                emitEvent(UserEvent.FinishLoading)
                emitEvent(UserEvent.UserLoadingFailed)
            }, {
                emitEvent(UserEvent.FinishLoading)
                emitEvent(UserEvent.UsersEmpty)
            })
            .toDisposables()
    }
}