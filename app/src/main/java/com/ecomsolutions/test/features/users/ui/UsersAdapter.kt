package com.ecomsolutions.test.features.users.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ecomsolutions.test.core.loadImage
import com.ecomsolutions.test.databinding.ItemUserBinding
import com.ecomsolutions.test.features.users.domain.User
import de.hdodenhof.circleimageview.CircleImageView

class UsersAdapter(
    private val items: List<User>,
) : RecyclerView.Adapter<UsersAdapter.UserViewHolder>() {

    private lateinit var binding: ItemUserBinding

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        binding = ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val item = items[position]
        holder.avatar.loadImage(item.avatarUrl)
        holder.name.text = item.fullName
    }

    inner class UserViewHolder(binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root) {
        var avatar: CircleImageView = binding.userAvatar
        var name: TextView = binding.name
    }
}