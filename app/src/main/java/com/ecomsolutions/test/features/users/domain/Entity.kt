package com.ecomsolutions.test.features.users.domain

data class User(
    val fullName: String,
    val avatarUrl: String
)