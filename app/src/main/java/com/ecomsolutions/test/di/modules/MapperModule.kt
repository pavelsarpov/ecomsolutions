package com.ecomsolutions.test.di.modules

import com.ecomsolutions.test.core.Mappers
import com.ecomsolutions.test.features.users.data.UserMapperImpl
import dagger.Binds
import dagger.Module

@Module
interface MapperModule {
    @Binds
    fun userMapper(mapper: UserMapperImpl): Mappers.UserMapper
}
