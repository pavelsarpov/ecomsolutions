package com.ecomsolutions.test.di.modules

import com.ecomsolutions.test.core.Interactors
import com.ecomsolutions.test.features.users.domain.UserInteractorImpl
import dagger.Binds
import dagger.Module

@Module
interface InteractorModule {
    @Binds
    fun usersInteractor(userInteractor: UserInteractorImpl): Interactors.UserInteractor
}
