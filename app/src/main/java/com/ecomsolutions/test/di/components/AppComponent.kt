package com.ecomsolutions.test.di.components

import com.ecomsolutions.test.App
import com.ecomsolutions.test.di.modules.*
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        ActivitiesModule::class,
        AndroidSupportInjectionModule::class,
        ApiModule::class,
        ApplicationModule::class,
        InteractorModule::class,
        FragmentsModule::class,
        MapperModule::class,
        PersistenceModule::class,
        RepositoryModule::class
    ]
)
@Singleton
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        fun applicationModule(applicationModule: ApplicationModule): Builder
        fun persistenceModule(persistenceModule: PersistenceModule): Builder

        fun build(): AppComponent
    }
}
