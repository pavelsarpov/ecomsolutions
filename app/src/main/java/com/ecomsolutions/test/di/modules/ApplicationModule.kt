package com.ecomsolutions.test.di.modules

import android.content.Context
import com.ecomsolutions.test.App
import dagger.Module
import dagger.Provides
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: App) {

    @Provides
    @Singleton
    fun application(): DaggerApplication = application

    @Provides
    @Singleton
    fun applicationContext(application: DaggerApplication): Context = application.applicationContext
}
