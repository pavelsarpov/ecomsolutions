package com.ecomsolutions.test.di.modules

import com.ecomsolutions.test.BuildConfig
import com.ecomsolutions.test.core.API
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class ApiModule {

    companion object {
        const val BASE_URL = "https://randomuser.me/"
    }

    @Provides
    fun gson(): Gson = GsonBuilder().setLenient().create()

    @Provides
    fun apiService(client: OkHttpClient, gson: Gson): API {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(API::class.java)
    }

    @Provides
    fun okHttpClient(): OkHttpClient {
        val listOfConnections = if (BuildConfig.DEBUG) {
            listOf(
                ConnectionSpec.MODERN_TLS,
                ConnectionSpec.RESTRICTED_TLS,
                ConnectionSpec.CLEARTEXT,
                ConnectionSpec.COMPATIBLE_TLS
            )
        } else {
            listOf(ConnectionSpec.COMPATIBLE_TLS)
        }
        return OkHttpClient.Builder()
            .connectionSpecs(listOfConnections)
            .connectTimeout(10, TimeUnit.MINUTES)
            .readTimeout(10, TimeUnit.MINUTES)
            .writeTimeout(10, TimeUnit.MINUTES)
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            ).addInterceptor {
                it.proceed(it.request().newBuilder().addHeader("Content-Type", "application/json").build())
            }
            .build()
    }
}