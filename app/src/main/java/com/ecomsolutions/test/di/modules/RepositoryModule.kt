package com.ecomsolutions.test.di.modules

import com.ecomsolutions.test.core.Repositories
import com.ecomsolutions.test.features.users.data.UserRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {
    @Binds
    fun userRepository(repository: UserRepositoryImpl): Repositories.UserRepository
}
