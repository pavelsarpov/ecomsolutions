package com.ecomsolutions.test.di.modules

import com.ecomsolutions.test.features.users.UserModule
import com.ecomsolutions.test.features.users.ui.UsersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentsModule {

    @ContributesAndroidInjector(modules = [UserModule::class])
    fun userFragment(): UsersFragment
}