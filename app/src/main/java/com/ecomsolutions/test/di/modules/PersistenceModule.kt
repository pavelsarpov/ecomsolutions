package com.ecomsolutions.test.di.modules

import android.content.Context
import androidx.room.Room
import com.ecomsolutions.test.cache.AppDataBase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class PersistenceModule(private val appContext: Context) {

    @Provides
    @Singleton
    fun room(): AppDataBase {
        return Room.databaseBuilder(appContext, AppDataBase::class.java, "database").build()
    }
}
